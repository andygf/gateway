using System.Threading.Tasks;
using GatewayApi.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GatewayApi.Controllers
{
    public partial class GatewaysController
    {
        // POST api/gateways/SN/devices
        [HttpPost("{sn}/devices")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Gateway>> CreateDeviceAsync(string sn, Device device)
        {
            var gateway = await _repository.GetGatewayAsync(sn);
            var conflict = await _repository.GetDeviceAsync(device.Uid);

            if (gateway == null)
                return NotFound();

            if (conflict != null)
                return Conflict();

            if (gateway.Devices.Count >= 10)
                return BadRequest();

            await _repository.AddDeviceAsync(gateway, device);

            return CreatedAtAction(nameof(GetBySnAsync),
                new {sn = gateway.Sn}, gateway);
        }


        // DELETE api/gateways/SN/devices/UID
        [HttpDelete("{sn}/devices/{uid}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<Gateway>> DeleteDeviceAsync(string sn, long uid)
        {
            var gateway = await _repository.GetGatewayAsync(sn);
            var device = await _repository.GetDeviceAsync(uid);

            if (gateway == null || device == null)
                return NotFound();

            if (device.GatewaySn != gateway.Sn)
                return NotFound();

            await _repository.RemoveDeviceAsync(device);

            return NoContent();
        }
    }
}