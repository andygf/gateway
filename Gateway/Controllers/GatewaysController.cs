using System.Collections.Generic;
using System.Threading.Tasks;
using GatewayApi.DataAccess.Contracts;
using GatewayApi.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GatewayApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public partial class GatewaysController : ControllerBase
    {
        private readonly IGatewaysRepository _repository;

        public GatewaysController(IGatewaysRepository repository)
        {
            _repository = repository;
        }

        // GET api/gateways/SN
        [HttpGet("{sn}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(Gateway), StatusCodes.Status200OK)]
        public async Task<ActionResult<Gateway>> GetBySnAsync(string sn)
        {
            var gateway = await _repository.GetGatewayAsync(sn);

            if (gateway == null)
                return NotFound();

            return Ok(gateway);
        }

        // GET api/gateways
        [HttpGet]
        [ProducesResponseType(typeof(List<Gateway>), StatusCodes.Status200OK)]
        public async Task<ActionResult<List<Gateway>>> GetAsync()
        {
            var gateways = await _repository.GetGatewaysAsync();

            return Ok(gateways);
        }

        // POST api/gateways
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Gateway>> CreateAsync(Gateway gateway)
        {
            if (!ModelState.IsValid)
                return BadRequest(gateway);

            var conflict = await _repository.GetGatewayAsync(gateway.Sn);

            if (conflict != null)
                return Conflict(gateway);

            await _repository.AddGatewayAsync(gateway);

            return CreatedAtAction(nameof(GetBySnAsync),
                new {sn = gateway.Sn}, gateway);
        }

        // DELETE api/gateways/SN
        [HttpDelete("{sn}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<Gateway>> DeleteAsync(string sn)
        {
            var gateway = await _repository.GetGatewayAsync(sn);

            if (gateway == null)
                return NotFound();

            await _repository.RemoveGatewayAsync(gateway);

            return NoContent();
        }

        // PUT: api/gateways/SN
        [HttpPut("{sn}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateAsync(string sn, Gateway gateway)
        {
            if (sn != gateway.Sn)
                return BadRequest(gateway);

            var old = await _repository.GetGatewayAsync(sn, false);

            if (old == null)
                return NotFound();

            await _repository.UpdateGatewayAsync(gateway);

            return NoContent();
        }
    }
}