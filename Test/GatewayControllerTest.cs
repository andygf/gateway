using System.Collections.Generic;
using GatewayApi.Controllers;
using GatewayApi.DataAccess.Contracts;
using GatewayApi.DataAccess.Models;
using Microsoft.AspNetCore.Mvc;
using Test.Mocks;
using Xunit;

namespace Test
{
    public class GatewayControllerTest
    {
        public GatewayControllerTest()
        {
            _repository = new GatewaysRepositoryFake();
            _controller = new GatewaysController(_repository);
        }

        private readonly GatewaysController _controller;
        private readonly IGatewaysRepository _repository;

        [Fact]
        public void Create_ExistingSnPassed_ReturnsConflictRequest()
        {
            // Arrange
            var testItem = new Gateway
            {
                Sn = "GW-000.001",
                Address = "127.0.0.1",
                Name = "Gateway Number 1"
            };

            // Act
            var badResponse = _controller.CreateAsync(testItem).Result.Result;

            // Assert
            Assert.IsType<ConflictObjectResult>(badResponse);
        }

        [Fact]
        public void Create_InvalidObjectPassed_ReturnsBadRequest()
        {
            // Arrange
            var nameMissingItem = new Gateway
            {
                Sn = "GW-000.003",
                Address = "127.0.0.3"
//                Name = "Gateway Number 3"
            };
            _controller.ModelState.AddModelError("Name", "Required");

            // Act
            var badResponse = _controller.CreateAsync(nameMissingItem).Result.Result;

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }


        [Fact]
        public void Create_ValidObjectPassed_ReturnedResponseHasCreatedItem()
        {
            // Arrange
            var testItem = new Gateway
            {
                Sn = "GW-000.004",
                Address = "127.0.0.4",
                Name = "Gateway Number 4"
            };

            // Act
            var createdResponse = _controller.CreateAsync(testItem).Result.Result as CreatedAtActionResult;
            var item = createdResponse.Value as Gateway;

            // Assert
            Assert.IsType<Gateway>(item);
            Assert.Equal("Gateway Number 4", item.Name);
        }


        [Fact]
        public void Create_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            var testItem = new Gateway
            {
                Sn = "GW-000.004",
                Address = "127.0.0.4",
                Name = "Gateway Number 4"
            };

            // Act
            var createdResponse = _controller.CreateAsync(testItem).Result.Result;

            // Assert
            Assert.IsType<CreatedAtActionResult>(createdResponse);
        }

        [Fact]
        public void Delete_ExistingSnPassed_RemovesOneItem()
        {
            // Arrange
            var existingSn = "GW-000.001";

            // Act
            var okResponse = _controller.DeleteAsync(existingSn).Result.Result;

            // Assert
            Assert.IsType<NoContentResult>(okResponse);
            Assert.Equal(1, _repository.GetGatewaysAsync().Result.Count);
        }

        [Fact]
        public void Delete_ExistingSnPassed_ReturnsNoContentResult()
        {
            // Arrange
            var existingSn = "GW-000.001";

            // Act
            var okResponse = _controller.DeleteAsync(existingSn).Result.Result;

            // Assert
            Assert.IsType<NoContentResult>(okResponse);
        }

        [Fact]
        public void Delete_NotExistingSnPassed_ReturnsNotFoundResponse()
        {
            // Arrange
            var notExistingSn = "i don't exist";

            // Act
            var badResponse = _controller.DeleteAsync(notExistingSn).Result.Result;

            // Assert
            Assert.IsType<NotFoundResult>(badResponse);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsAllItems()
        {
            // Act
            var okResult = _controller.GetAsync().Result.Result as OkObjectResult;

            // Assert
            var items = Assert.IsType<List<Gateway>>(okResult.Value);
            Assert.Equal(2, items.Count);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.GetAsync().Result;

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetBySn_WhenCalled_ReturnsCorrectItem()
        {
            // Act
            var okResult = _controller.GetBySnAsync("GW-000.001").Result.Result as OkObjectResult;

            // Assert
            var item = Assert.IsType<Gateway>(okResult.Value);
            Assert.Equal("GW-000.001", item.Sn);
        }

        [Fact]
        public void GetBySn_WhenCalledWithExistingSn_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.GetBySnAsync("GW-000.001").Result;

            // Assert
            Assert.IsType<OkObjectResult>(okResult.Result);
        }

        [Fact]
        public void GetBySn_WhenCalledWithNonExistingSn_ReturnsNotFoundResult()
        {
            // Act
            var result = _controller.GetBySnAsync("GW-000.009").Result;

            // Assert
            Assert.IsType<NotFoundResult>(result.Result);
        }

        [Fact]
        public void Update_ChangingSn_ReturnsBadRequestResponse()
        {
            // Arrange
            var notExistingSn = "i don't exist";

            // Act
            var badResponse = _controller.UpdateAsync(notExistingSn, new Gateway()).Result;

            // Assert
            Assert.IsType<BadRequestObjectResult>(badResponse);
        }

        [Fact]
        public void Update_ExistingSnPassed_ReturnsNoContentResult()
        {
            // Arrange
            var existingSn = "GW-000.002";

            // Act
            var okResponse = _controller.UpdateAsync(existingSn, new Gateway
            {
                Sn = "GW-000.002",
                Name = "New name",
                Address = "New Address"
            }).Result;

            // Assert
            Assert.IsType<NoContentResult>(okResponse);
        }

        [Fact]
        public void Update_NotExistingSnPassed_ReturnsNotFoundResponse()
        {
            // Arrange
            var notExistingSn = "i don't exist";

            // Act
            var badResponse = _controller.UpdateAsync(notExistingSn, new Gateway {Sn = "i don't exist"}).Result;

            // Assert
            Assert.IsType<NotFoundResult>(badResponse);
        }
    }
}