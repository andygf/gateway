using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GatewayApi.DataAccess.Contracts;
using GatewayApi.DataAccess.Models;

namespace Test.Mocks
{
    public class GatewaysRepositoryFake : IGatewaysRepository
    {
        private readonly List<Gateway> _gateways;

        public GatewaysRepositoryFake()
        {
            var rnd = new Random();
            _gateways = new List<Gateway>
            {
                new Gateway
                {
                    Sn = "GW-000.001",
                    Address = "127.0.0.1",
                    Name = "Gateway Number 1"
                },
                new Gateway
                {
                    Sn = "GW-000.002",
                    Address = "127.0.0.2",
                    Name = "Gateway Number 2"
                }
            };

            for (var i = 0; i < 10; i++)
                _gateways[0].Devices.Add(new Device
                {
                    Gateway = _gateways[0],
                    GatewaySn = _gateways[0].Sn,
                    Uid = rnd.Next(),
                    Status = DeviceStatus.Online,
                    Vendor = "Device " + i
                });
        }

        public async Task<List<Gateway>> GetGatewaysAsync()
        {
            return _gateways;
        }

        public async Task<Gateway> GetGatewayAsync(string sn, bool tracking = true)
        {
            return _gateways.Find(g => g.Sn == sn);
        }

        public async Task<int> AddGatewayAsync(Gateway gateway)
        {
            _gateways.Add(gateway);
            return 1;
        }

        public async Task<int> RemoveGatewayAsync(Gateway gateway)
        {
            _gateways.RemoveAll(g => g.Sn == gateway.Sn);
            return 1;
        }

        public async Task<Gateway> UpdateGatewayAsync(Gateway gateway)
        {
            _gateways.RemoveAll(g => g.Sn == gateway.Sn);
            _gateways.Add(gateway);
            return gateway;
        }

        public async Task<int> AddDeviceAsync(Gateway gateway, Device device)
        {
            gateway = _gateways.Find(g => g.Sn == gateway.Sn);
            gateway.Devices.Add(device);
            return 1;
        }

        public async Task<Device> GetDeviceAsync(long uid)
        {
            foreach (var gateway in _gateways)
            {
                var device = gateway.Devices.Find(d => d.Uid == uid);
                if (device != null)
                    return device;
            }

            return null;
        }

        public async Task<int> RemoveDeviceAsync(Device device)
        {
            foreach (var gateway in _gateways) gateway.Devices.RemoveAll(d => d.Uid == device.Uid);

            return 1;
        }
    }
}