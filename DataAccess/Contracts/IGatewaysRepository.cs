using System.Collections.Generic;
using System.Threading.Tasks;
using GatewayApi.DataAccess.Models;

namespace GatewayApi.DataAccess.Contracts
{
    public interface IGatewaysRepository
    {
        Task<List<Gateway>> GetGatewaysAsync();
        Task<Gateway> GetGatewayAsync(string sn, bool tracking = true);
        Task<int> AddGatewayAsync(Gateway gateway);
        Task<int> RemoveGatewayAsync(Gateway gateway);
        Task<Gateway> UpdateGatewayAsync(Gateway gateway);
        Task<int> AddDeviceAsync(Gateway gateway, Device device);
        Task<Device> GetDeviceAsync(long uid);
        Task<int> RemoveDeviceAsync(Device device);
    }
}