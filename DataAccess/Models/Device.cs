using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace GatewayApi.DataAccess.Models
{
    public class Device
    {
        public Device()
        {
            CreatedAt = DateTime.Now;
        }

        [Key] public long Uid { get; set; }

        [Required] public string Vendor { get; set; }

        [DataType(DataType.Date)] public DateTime CreatedAt { get; set; }

        [EnumDataType(typeof(DeviceStatus))] public DeviceStatus Status { get; set; }

        public string GatewaySn { get; set; }

        [ForeignKey("GatewaySn")] [JsonIgnore] public Gateway Gateway { get; set; }
    }

    public enum DeviceStatus
    {
        Offline = 0,
        Online = 1
    }
}