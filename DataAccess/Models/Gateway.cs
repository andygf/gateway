using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;
using Newtonsoft.Json;

namespace GatewayApi.DataAccess.Models
{
    public class Gateway
    {
        [Key] [Required] public string Sn { get; set; }

        [Required] public string Name { get; set; }

        [Required]
        [RegularExpression(
            "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")]
        public string Address { get; set; }

        [NotMapped]
        [JsonIgnore]
        public virtual IPAddress IpAddress
        {
            get => IPAddress.Parse(Address);
            set => Address = value.ToString();
        }

        [MaxLength(10)] public virtual List<Device> Devices { get; set; } = new List<Device>();
    }
}