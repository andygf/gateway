using GatewayApi.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace GatewayApi.DataAccess
{
    public class GatewayApiContext : DbContext
    {
        public GatewayApiContext(DbContextOptions<GatewayApiContext> options)
            : base(options)
        {
        }

        public DbSet<Gateway> Gateways { get; set; }
        public DbSet<Device> Devices { get; set; }
    }
}