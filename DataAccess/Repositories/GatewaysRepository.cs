using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GatewayApi.DataAccess.Contracts;
using GatewayApi.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace GatewayApi.DataAccess.Repositories
{
    public class GatewaysRepository : IGatewaysRepository
    {
        private readonly GatewayApiContext _context;

        public GatewaysRepository(GatewayApiContext context)
        {
            _context = context;
        }

        public async Task<List<Gateway>> GetGatewaysAsync()
        {
            return await _context.Gateways.Include(g => g.Devices).AsNoTracking().ToListAsync();
        }

        public async Task<Gateway> GetGatewayAsync(string sn, bool tracking = true)
        {
            IQueryable<Gateway> query = _context.Gateways.Where(g => g.Sn == sn).Include(g => g.Devices);
            if (!tracking)
                query = query.AsNoTracking();
            return await query.FirstOrDefaultAsync();
        }

        public async Task<int> AddGatewayAsync(Gateway gateway)
        {
            var rowsAffected = 0;

            _context.Gateways.Add(gateway);
            rowsAffected = await _context.SaveChangesAsync();

            return rowsAffected;
        }

        public async Task<int> RemoveGatewayAsync(Gateway gateway)
        {
            var rowsAffected = 0;

            _context.Gateways.Remove(gateway);
            rowsAffected = await _context.SaveChangesAsync();

            return rowsAffected;
        }

        public async Task<Gateway> UpdateGatewayAsync(Gateway gateway)
        {
            _context.Entry(gateway).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return gateway;
        }

        public async Task<int> AddDeviceAsync(Gateway gateway, Device device)
        {
            var rowsAffected = 0;

            gateway.Devices.Add(device);
            rowsAffected = await _context.SaveChangesAsync();

            return rowsAffected;
        }

        public async Task<Device> GetDeviceAsync(long uid)
        {
            return await _context.Devices.FindAsync(uid);
        }

        public async Task<int> RemoveDeviceAsync(Device device)
        {
            var rowsAffected = 0;

            _context.Devices.Remove(device);
            rowsAffected = await _context.SaveChangesAsync();

            return rowsAffected;
        }
    }
}